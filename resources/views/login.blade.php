@extends('layouts.app')
@section('title', 'Login')
@section('content')
<!-- Content -->
<div id="content">

	<!-- Section -->
	<section class="bg-black fullheight dark">

		<div class="bg-image"><img src="assets/img/photos/classic_bg03.jpg" alt=""></div>

		<div class="container v-center text-center">
			<div class="row">
				<div class="col-lg-4 col-lg-push-4">
					<div class="bordered-box rounded animated" data-animation="fadeInDown">
						<h2>Log in!</h2>
						<form id="login-form" class="validate-form text-center mb-30">
							<div class="form-group mb-10">
								<label for="login">Login:</label>
								<input id="login" name="login" type="text" class="form-control input-2" required>
							</div>
							<div class="form-group mb-10">
								<label for="password">Password:</label>
								<input id="password" name="password" type="text" class="form-control input-2" required>
							</div>
							<button type="submit" class="btn btn-filled btn-primary btn-block">Login</button>
						</form>
						<a href="#" class="link-underline">Forgot my password</a>
					</div>
				</div>
			</div>
		</div>

	</section>
</div>
<!-- Content / End -->

@endsection
