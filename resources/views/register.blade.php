@extends('layouts.app')
@section('title', 'Register')
@section('content')
<!-- Content -->
<div id="content">

	<!-- Section -->
	<section class="bg-black fullheight dark">

		<div class="bg-image"><img src="assets/img/photos/classic_bg04.jpg" alt=""></div>

		<div class="container v-center">
			<div class="row">
				<div class="col-lg-10 col-lg-push-1">
					<div class="bordered-box rounded">
						<h2>Register now!</h2>
						<form id="login-form" class="validate-form">
							<div class="row">
								<div class="col-md-6 form-group mb-10">
									<label for="name">Name &amp; surename:</label>
									<input id="name" name="name" type="text" class="form-control" required>
								</div>
								<div class="col-md-6 form-group mb-10">
									<label for="login">Login:</label>
									<input id="login" name="login" type="text" class="form-control" required>
								</div>
								<div class="col-md-6 form-group mb-10">
									<label for="password">Password:</label>
									<input id="password" name="password" type="text" class="form-control" required>
								</div>
								<div class="col-md-6 form-group mb-10">
									<label for="password-repeat">Repeat password:</label>
									<input id="password-repeat" name="password-repeat" type="text" class="form-control" required>
								</div>
								<div class="col-md-6 form-group mb-10">
									<label for="email">E-mail:</label>
									<input id="email" name="email" type="email" class="form-control" required>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 col-md-push-3">
									<button class="btn btn-filled btn-primary btn-block">Register</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

	</section>

</div>
<!-- Content / End -->


@endsection
