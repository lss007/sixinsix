@extends('layouts.app')
@section('title', 'Careers')
@section('content')

<!-- Page Title -->
<div id="page-title" class="page-title page-title-2 bg-black dark">
	<div class="bg-image"><img src="{{ asset('assets/img/photos/classic_title01.jpg') }}" alt=""></div>
	<div class="container text-center">
		<h1>Careers</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/') }}">Home Page</a></li>
			<li class="active">creers</li>
		</ol>
	</div>
</div>
<!-- Page Title / End -->


<!-- Content -->
<div id="content">

	<!-- Section-->
	<section>
		<div class="container">
			<h3 class="mb-50 text-center">Check our current openings!</h3>
			<div class="row">
				<div class="col-md-4">
					<!-- Career Group -->
					<div class="career-group">
						<div class="career-group-title">Engineering</div>
						<!-- Career -->
						<div class="career">
							<h5 class="title">Senior Full Stack Developer</h5>
							<ul class="details list-inline">
								<li><i class="i-before fa fa-map-marker text-primary"></i><span class="text-muted">Los Angeles, California, USA</span></li>
								<li><i class="i-before fa fa-clock-o text-primary"></i><span class="text-muted">2016.05.01</span></li>
							</ul>
						</div>
						<!-- Career -->
						<div class="career">
							<h5 class="title">Front-End Developer</h5>
							<ul class="list-inline">
								<li><i class="i-before fa fa-map-marker text-primary"></i><span class="text-muted">Los Angeles, California, USA</span></li>
								<li><i class="i-before fa fa-clock-o text-primary"></i><span class="text-muted">2016.05.01</span></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<!-- Career Group -->
					<div class="career-group">
						<div class="career-group-title">Marketing</div>
						<!-- Career -->
						<div class="career">
							<h5 class="title">Social Marketing Specialist</h5>
							<ul class="details list-inline">
								<li><i class="i-before fa fa-map-marker text-primary"></i><span class="text-muted">Los Angeles, California, USA</span></li>
								<li><i class="i-before fa fa-clock-o text-primary"></i><span class="text-muted">2016.05.01</span></li>
							</ul>
						</div>
					</div>
					<!-- Career Group -->
					<div class="career-group">
						<div class="career-group-title">Managing</div>
						<!-- Career -->
						<div class="career">
							<h5 class="title">Account Manager</h5>
							<ul class="details list-inline">
								<li><i class="i-before fa fa-map-marker text-primary"></i><span class="text-muted">Los Angeles, California, USA</span></li>
								<li><i class="i-before fa fa-clock-o text-primary"></i><span class="text-muted">2016.05.01</span></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<!-- Career Group -->
					<div class="career-group">
						<div class="career-group-title">Graphic</div>
						<!-- Career -->
						<div class="career">
							<h5 class="title">Art Director</h5>
							<ul class="details list-inline">
								<li><i class="i-before fa fa-map-marker text-primary"></i><span class="text-muted">Los Angeles, California, USA</span></li>
								<li><i class="i-before fa fa-clock-o text-primary"></i><span class="text-muted">2016.05.01</span></li>
							</ul>
						</div>
						<!-- Career -->
						<div class="career">
							<h5 class="title">Junior UI/UX Designer</h5>
							<ul class="list-inline">
								<li><i class="i-before fa fa-map-marker text-primary"></i><span class="text-muted">Los Angeles, California, USA</span></li>
								<li><i class="i-before fa fa-clock-o text-primary"></i><span class="text-muted">2016.05.01</span></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Section-->
	<section class="bg-grey">
		<div class="bg-icons white">
			<i class="ti-search"></i>
		</div>
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-8 col-lg-push-2">
					<h3 class="mb-50">We are always looking for talented people - send Us your resume and something awesome you have made!</h3>
					<a href="{{ url('/contact') }}" class="btn btn-filled btn-primary">Contact Us <i class="i-after ti-arrow-right"></i></a>
				</div>
			</div>
		</div>
	</section>


	<!-- Section-->
	<section class="bg-secondary dark text-center border-bottom">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-lg-push-3">
					<h2>Whould like to use this template with your next project?</h2>
					<div class="row">
						<div class="col-sm-6"><a href="#" class="btn btn-primary btn-filled btn-block">Yes, want to buy it now!</a></div>
						<div class="col-sm-6"><a href="#" class="btn btn-link btn-block">Check documentation</a></div>
					</div>
				</div>
			</div>
		</div>
	</section>

</div>
<!-- Content / End -->

@endsection
