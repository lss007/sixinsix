<!DOCTYPE html>
<html lang="en" data-cookies-popup="true">
  <head>
    <!-- Meta -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Title -->
    <title>SixInSix - @yield('title')</title>

    <!-- Favicons -->
    <link rel="shortcut icon" href="{{ asset('assets/img/favicon.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/favicon_76x76.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/img/favicon_120x120.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/img/favicon_152x152.png') }}">

    <!-- Google Web Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,200,300,100,500,600,700' rel='stylesheet' type='text/css'>

    <!-- CSS Styles -->
    <link rel="stylesheet" href="{{ asset('assets/css/styles.css') }}" />

    <!-- CSS Base -->
    <link id="theme" rel="stylesheet" href="{{ asset('assets/css/themes/theme-teal.css') }}" />

  </head>
<body class="one-page header-absolute">
<!-- Loader -->
<div id="page-loader"><span class="loader-2 loader-primary"></span></div>
<!-- Loader / End -->
<!-- Header -->
<header id="header" class="absolute fullwidth transparent">

	<!-- Navigation Bar -->
	<div id="nav-bar">

		<!-- Logo -->
		<a class="logo-wrapper" href="{{ url('/') }}">
			<img class="logo logo-light" src="{{ asset('assets/img/logo-sixinsix.png') }}" alt="Sixinsix">
      <img class="logo logo-dark" src="{{ asset('assets/img/logo-dark.png') }}" alt="Okno">
		</a>

		<nav class="module-group right">

			<!-- Primary Menu -->
			<div class="module menu left">
				<ul id="nav-primary" class="nav nav-primary">
  					<li><a href="{{ url('/') }}">Home</a></li>
					<li><a href="{{ url('/about') }}">About</a></li>
					<li><a href="{{ url('/services') }}">Services</a></li>
					<li><a href="{{ url('/portfolios') }}">Portfolio</a></li>
					<li><a href="{{ url('/faqs') }}">Faq</a></li>
          <li><a href="{{ url('/careers') }}">Careers</a></li>
					<li><a href="{{ url('/blog') }}">Blog</a></li>
          <li><a href="{{ url('/contact') }}">Contact</a></li>
				</ul>
			</div>
		</nav>
	</div>

	<!-- Notification Bar -->
	<div id="notification-bar"></div>
  </header>
<!-- Header / End -->
   @yield('content')

   <!-- Section / Contact -->
 <section id="contact" class="bg-secondary dark border-bottom">

   <div class="container text-center">
     <div class="row text-center">
       <div class="col-lg-8 col-lg-push-2">
         <div class="row v-center-items">
           <div class="col-md-4 col-md-push-4">
             <img src="{{ asset('assets/img/logo-sixinsix.png   ') }}" alt="" class="mb-30">
           </div>
           <div class="col-md-4 col-md-pull-4">
             <div class="feature feature-1">
               <span class="icon icon-primary icon-sm"><i class="ti-mobile"></i></span>
               <h6>+48 210 232 322</h6>
             </div>
           </div>
           <div class="col-md-4">
             <div class="feature feature-1">
               <span class="icon icon-primary icon-sm"><i class="ti-email"></i></span>
               <h6><a href="#">mail@example.com</a></h6>
             </div>
           </div>
         </div>
       </div>
     </div>
     <div class="mt-40 text-muted">
       Copyright Okno 2016©. All rights reserved.<br>
       Made with <i class="fa fa-heart text-primary"></i> by <a href="http://www.suelo.pl" target="_blank">suelo.pl</a>
     </div>
   </div>

 </section>

</div>
<!-- Content / End -->
<!-- JS Libraries -->
<script src="{{ asset('assets/js/jquery-1.12.3.min.js') }}"></script>
<!-- JS Plugins -->
<script src="{{ asset('assets/js/plugins.js') }}"></script>
<!-- JS Core -->
<script src="{{ asset('assets/js/core.js') }}"></script>
<!-- JS Google Map -->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
</body>
</html>
