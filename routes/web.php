<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/about', 'AboutController@index')->name('about-us');
Route::get('/blog', 'BlogController@index')->name('blog');
Route::get('/contact', 'ContactController@index')->name('contact-us');
Route::get('/faqs', 'FaqController@index')->name('faqs');
Route::get('/login', 'LoginController@index')->name('login');
Route::get('/register', 'RegisterController@index')->name('register');
Route::get('/portfolios', 'PortfolioController@index')->name('portfolios');
Route::get('/services', 'ServiceController@index')->name('services');
Route::get('/careers', 'CareerController@index')->name('careers');
